var myChart = echarts.init(document.getElementById('my-Echart'));//堆叠区域图
var thisURL = document.URL;
var storeId=getarg(thisURL);//获取公司ID
var m_1=m-1; 
var y_1=y-1; 
//折线图属性
option = {
    title : {
        text: '',
        x:'center',
        textStyle: {
            fontWeight: 'normal',              //标题颜色
            color: '#333333',
            fontSize:14
        }
    },
    grid: {
        left: '3%',
        right: '3%',
        bottom: '20%',
        top:'15%',
        containLabel: true
    },
    tooltip : {
        formatter:'<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:#c23531"></span>'+m+'月{b}日：{c0}万<br/><span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:#2f4554"></span>'+m_1+'月{b1}日：{c1}万<br/><span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:#61a0a8"></span>'+y_1+'年'+m_1+'月{b2}日：{c2}万',
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    legend: {
        center: 'center',
        data: ['1','2','3'],
        textStyle:{    //图例文字的样式
            color:'#555',
            fontSize:12
        },
        bottom: 10,
        left: 'center'
    },
    xAxis: {
        type: 'category',
        boundaryGap : false,
        data: [],
        // 控制网格线是否显示
        splitLine:{
            show: false, 
            //  改变轴线颜色
            lineStyle: {
                // 使用深浅的间隔色
                color: ['#e3e3e3']
            }                            
        },
        //  改变x轴颜色
        axisLine:{
            lineStyle:{
                color:'#aeb6bb',
            }
        },                         
        //  改变x轴字体颜色和大小
        axisLabel: {
            textStyle: {
                color: '#aeb6bb',
                fontSize:'10'
            },
        } 
    },
    yAxis: {
        type: 'value',
        //  改变x轴颜色
        axisLine:{
            show: true,
            lineStyle:{
                color:'#e3e3e3',
            }
        },                         
        //  改变y轴字体颜色和大小
        axisLabel: {
            textStyle: {
                color: '#aeb6bb',
                fontSize:'10'
            },
            formatter: '{value}万'
        }
    },
    series: [
        {
            name: '1',
            type: 'line',
            areaStyle: {normal: {}},
            data: []
        },
        {
            name: '2',
            type: 'line',
            areaStyle: {normal: {}},
            data: [] 
        },
        {
            name: '3',
            type: 'line',
            areaStyle: {normal: {}},
            data: []
        }
    ]
};

$(function(){
    Cday(currentDay)   
    t = setInterval(showAuto, 1000); 
})
//-----------------------------------method--------------------------------------------------------------
// prev
function prevDay(){
    addDate($('#date').val(),-1);
    Cday($('#date').val())
}
//next
function nextDay(){
    addDate($('#date').val(),1);
    Cday($('#date').val())
}

//初始化请求数据
function Cday(dayData){
    console.info(dayData)
    $('#date').val(dayData);
    $.ajax({  
        type : "POST",  //提交方式
        url : market_statement_url,//路径  
        data : {  
            day : dayData,
            store_id : storeId
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result) 
            if(result.errcode==1){
                $('#brankTit').text(result.data.company_name);
                $('.yd-all-data div:nth-child(2)').text(result.data.signPeople)
                $('.yd-all-data div:nth-child(3)').text(result.data.countPeople)
                $('.yd-all-data div:nth-child(4)').text(result.data.newClient)
                $('.yd-all-data div:nth-child(5)').text(result.data.allEffective)
                $('.yd-all-data div:nth-child(6)').text(result.data.notSign)
                var this_package= result.data.this_package;
                var last_package= result.data.last_package;
                var last_year_package= result.data.last_year_package;
                var table_list=result.data.list;
                var create_dateArr_1 = new Array;
                var sum_package_moneyArr_1= new Array;
                var create_dateArr_2 = new Array;
                var sum_package_moneyArr_2= new Array;
                var create_dateArr_3 = new Array;
                var sum_package_moneyArr_3= new Array;
                // if(actual_priceList.length==0){
                //     window.YDUI.dialog.toast('暂无数据', 'error', 1000,function(){
                //         moneyArr.splice(0,moneyArr.length);//清空数组
                //         payTimeArr.splice(0,payTimeArr.length);
                //     });
                // }
                $('.yd-Echart p').html('<b style="font-weight: normal; color:#303030;font-size:.28rem">'+m+'</b>月销售业绩趋势对比')
                $('#my-Echart div:nth-child(2)').css('left','0')
                $('.yd-body .yd-tr-data-list').remove();
                for(var i=0;i<table_list.length;i++){
                    if(table_list[i].not_sign==undefined){
                       table_list[i].not_sign=0 
                    }
                    var $html='<div class="yd-tr yd-tr-data-list">' +
                                    '<div>'+table_list[i].user+'</div>' +
                                    '<div>'+table_list[i].sign_man+'</div>' +
                                    '<div>'+table_list[i].arrive+'</div>' +
                                    '<div>'+table_list[i].add_member+'</div>' +
                                    '<div>'+table_list[i].effective+'</div>' +
                                    '<div>'+table_list[i].not_sign+'</div>' +
                                '</div>';
                    $('.yd-body').append($html)                    
                }
                for(var i=0;i<this_package.length;i++){
                    create_dateArr_1.push(this_package[i].create_date);  
                    sum_package_moneyArr_1.push((parseFloat(this_package[i].sum_package_money)/10000).toFixed(2));  
                }
                for(var i=0;i<last_package.length;i++){
                    create_dateArr_2.push(last_package[i].create_date);  
                    sum_package_moneyArr_2.push((parseFloat(last_package[i].sum_package_money)/10000).toFixed(2));  
                }
                for(var i=0;i<last_year_package.length;i++){
                    create_dateArr_3.push(last_year_package[i].create_date);  
                    sum_package_moneyArr_3.push((parseFloat(last_year_package[i].sum_package_money)/10000).toFixed(2));  
                }
                option.title.text=m+'月销售业绩趋势对比'
                option.xAxis.data=create_dateArr_1;//赋值到图表的数组
                option.series[0].data=sum_package_moneyArr_1;//赋值到图表的数组
                option.xAxis.data=create_dateArr_2;//赋值到图表的数组
                option.series[1].data=sum_package_moneyArr_2;//赋值到图表的数组
                option.xAxis.data=create_dateArr_3;//赋值到图表的数组
                option.series[2].data=sum_package_moneyArr_3;//赋值到图表的数组

                var thisMonthMoney=eval(sum_package_moneyArr_1.join('+'));
                var prevMonthMoney=eval(sum_package_moneyArr_2.join('+'));
                var prevYearMoney=eval(sum_package_moneyArr_3.join('+'));
                var legendDataArr=[];
                legendDataArr.push('本月：'+thisMonthMoney.toFixed(2)+'万')
                legendDataArr.push('上月：'+prevMonthMoney.toFixed(2)+'万')
                legendDataArr.push('去年本月：'+prevYearMoney.toFixed(2)+'万')
                option.legend.data=legendDataArr;
                option.series[0].name='本月：'+thisMonthMoney.toFixed(2)+'万'
                option.series[1].name='上月：'+prevMonthMoney.toFixed(2)+'万'
                option.series[2].name='去年本月：'+prevYearMoney.toFixed(2)+'万'
                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                // 限制next按鈕不能超出當前時間，超出隱藏按鈕，否則顯示
                if($('#date').val()==currentDay){
                    $('.nextDay').hide()
                }else{
                    $('.nextDay').show()
                }
            }else{
                window.YDUI.dialog.toast(result.errinfo, 'error', 2000);
            } 
        }  
    });
}
