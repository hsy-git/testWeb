var myChart_1 = echarts.init(document.getElementById('my-Echart-1'));// 柱状图
var myChart_2 = echarts.init(document.getElementById('my-Echart-2'));//饼图
var thisURL = document.URL;
var storeId=getarg(thisURL);//获取公司ID
var getWeek=0;
var oneLoadNum = 0;
var t,n,count = 0;
$(function(){
    t = setInterval(showAuto, 1000); 
    $('#date').val(currentDay)//初始化获取当天日期
    Cday(currentDay)//初始化请求当天数据
    //点击日，周，月，年
    $('.yd-select-date button').on('click',function(){
        oneLoadNum=1;
        ToggleTab($(this));
        var index=$(this).index();
        // 点击日按钮
        if(index==0){
            $('.currentStatus').text('日');
            Cday(currentDay);//请求日数据
        // 点击周按钮    
        }else if(index==1){
            $('.currentStatus').text('周');
            get_week()//请求周数据
        // 点击月按钮  
        }else if(index==2){
            $('.currentStatus').text('月');
            Cmonth(y+"-"+m)//请求月数据
        // 点击年按钮
        }else if(index==3){
            $('.currentStatus').text('年');
            Cyear(y)//请求年数据
        }
    })
})
//柱状图
var option_1 = {
    tooltip: {
        trigger: 'item',
        formatter: '{b} : {c}万'
    },
     grid: {
        left: '2%',
        right: '4%',
        bottom: '3%',
        top:'10%',
        containLabel: true
    },
     xAxis: [
         {
            type: 'category',
            data : ['项目套餐','项目销售','月子套餐','商品销售','退款金额'],
             // 控制网格线是否显示
            splitLine:{
                show: false               
            }, 
            //设置轴线的属性
            axisLine:{
                lineStyle:{
                    color:'#aeb6bb',
                    width:1,//这里是为了突出显示加上的
                }
            },
            axisLabel:{
               interval:0,
               margin:5,
               textStyle: {
                    color: '#aeb6bb',
                    fontSize:'10'
                },        
           } 
         }
     ],
     yAxis: [
         {
            type: 'value',
            // 控制网格线是否显示
            splitLine:{
                show: true, 
                //  改变轴线颜色
                lineStyle: {
                    // 使用深浅的间隔色
                    color: ['#e3e3e3']
                }                            
            },
            //  改变y轴字体颜色和大小
            axisLabel: {
                textStyle: {
                    color: '#aeb6bb',
                    fontSize:'10'
                },
                formatter: '{value}万'
            }, 
            //设置轴线的属性
            axisLine:{
                lineStyle:{
                    color:'#aeb6bb',
                    width:1,//这里是为了突出显示加上的
                }
            }  
         }
     ],
     series: [
         {
             "name": "营业额",
             "type": "bar",
             "data": [],
             itemStyle: {
                normal: {
                    color: function(params) {
                        var colorList = [
                          '#a1d568','#a1d568','#a1d568','#a1d568','#eb4958'
                        ];
                        return colorList[params.dataIndex]
                    },
                    label: {  
                        show: true,//是否展示 
                        formatter: '{c}万',
                        position:'top',
                        textStyle: {   
                            fontSize : '10' 
                        }  
                    } 
                }
            },
         }
    ]
  };
  // 饼图
  option_2 = {
    // tooltip: {
    //     trigger: 'item',
    //     formatter: '{b} : {c}单'
    // },
    title: {
        text: '',
        textStyle:{
            color:'#4ec5b4',
            fontWeight:'normal',
            fontSize:'16px'
        },
        color:'#999',
        x: 'center',
        y: 'center'
    },
    color:[
          '#8a2983','#c5214f','#e56129','#f4b824','#a5c532',
           '#1ea093','#2b8cb9','#1a4894','#F3A43B','#60C0DD',
           '#D7504B','#C6E579','#F4E001','#F0805A','#26C0C0',
           'red', 'green','yellow','blueviolet'
        ],
    series: [
        {
            name:'套餐分布',
            type:'pie',
            radius: ['25%', '35%'],
            data:[],
            formatter: function(b){
                if(b.data.name.indexOf(")")>0){
                    return insert_flg(b.data.name,'\n',b.data.name.indexOf(")"));
                }
                return b.data.name;
            },
            itemStyle: {
                normal:{
                    label:{
                        show:true,
                        //formatter: '{b}'
                    },
                    labelLine:{
                        show:true
                    }
                }
            },
        }
    ]
};
//-----------------------------------method--------------------------------------------------------------

// prev
function prevDay(){
    //如果是日
    if($('.c_day').hasClass('active')){
        addDate($('#date').val(),-1);
        Cday($('#date').val())
        return
    }
    //如果是周
    if($('.c_week').hasClass('active')){
        var getWeek=parseInt($('input[name=getCurrentWeek]').val())-1;
        transformAjax(getWeek)
        return
    }
    //如果是月
    if($('.c_month').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
        var str=dateToString(AddMonths(myDate,-1));
        var getMonth=str.substr(0,7);
        Cmonth(getMonth)
        return
    }
    //如果是年
    if($('.c_year').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
        var str=dateToString(AddYear(myDate,-1));
        var getYear=str.substr(0,4);
        Cyear(getYear)
        return
    }
}
//next
function nextDay(){
    //如果是日
    if($('.c_day').hasClass('active')){
        addDate($('#date').val(),1);
        Cday($('#date').val())
        return
    }
    //如果是周
    if($('.c_week').hasClass('active')){
        var getWeek=parseInt($('input[name=getCurrentWeek]').val())+1;
        transformAjax(getWeek)
        return
    }
    //如果是月
    if($('.c_month').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
        var str=dateToString(AddMonths(myDate,1));
        var getMonth=str.substr(0,7);
        Cmonth(getMonth)
        return
    }
    //如果是年
    if($('.c_year').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
        var str=dateToString(AddYear(myDate,1));
        var getYear=str.substr(0,4);
        Cyear(getYear)
        return
    }
}
 //月份增减
function AddMonths(date, value) {
    date.setMonth(date.getMonth() + value);
    return date;
}
 //年份增减
function AddYear(date, value) {
    date.setFullYear(date.getFullYear() + value);
    return date;
}
//时间格式转化字符串
function dateToString(now){  
    var year = now.getFullYear();  
    var month =(now.getMonth() + 1).toString();  
    var day = (now.getDate()).toString();  
    var hour = (now.getHours()).toString();  
    var minute = (now.getMinutes()).toString();  
    var second = (now.getSeconds()).toString();  
    if (month.length == 1) {  
        month = "0" + month;  
    }  
    if (day.length == 1) {  
        day = "0" + day;  
    }  
    if (hour.length == 1) {  
        hour = "0" + hour;  
    }  
    if (minute.length == 1) {  
        minute = "0" + minute;  
    }  
    if (second.length == 1) {  
        second = "0" + second;  
    }  
     var dateTime = year + "-" + month + "-" + day +" "+ hour +":"+minute+":"+second;  
     return dateTime;  
}
//切换周期
function transformAjax(Week){
    var 
        date=$('#date').val();
        Sweek=date.substr(0,10);
        Eweek=date.substr(11,10);
    $.ajax({  
        type : "POST",  //提交方式
        url : get_week_url,//路径  
        data : {
            week: Week,
            sweek:Sweek,
            eweek:Eweek

        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            if(result.errcode==1){
                var getWeek=result.data.week;
                $('input[name=getCurrentWeek]').val(getWeek)
            }
            var sweek=result.data.weeks[0];
            var eweek=result.data.weeks[1];
            $('#date').val(sweek+'至'+eweek);
            Cweek($('#date'))//周请求数据
        }  
    });
}

//日请求数据
function Cday(dayData){
    console.info(dayData)
    $('#date').val(dayData);
    $.ajax({  
        type : "POST",  //提交方式
        url : business_statement_url,//路径  
        data : {  
            day : dayData,
            store_id : storeId
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}
//获取周
function get_week(){
    $.ajax({  
        type : "POST",  //提交方式
        url : get_week_url,//路径  
        data : { 
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            if(result.errcode==1){
                getWeek=result.data.week;
                $('input[name=getCurrentWeek]').val(getWeek)
            }
            var sweek_0=result.data.weeks[0];
            var eweek_1=result.data.weeks[1];
            $('#date').val(sweek_0+'至'+eweek_1);
            Cweek($('#date'))//周请求数据
        }  
    });
}
//周请求数据
function Cweek(obj){    
    console.info(obj.val())
    var 
        globalWeek=obj.val();
        Sweek=globalWeek.substr(0,10);
        Eweek=globalWeek.substr(11,10);
    $.ajax({  
        type : "POST",  //提交方式
        url : business_statement_url,//路径  
        data : {  
            sweek:Sweek,
            eweek:Eweek,
            store_id : storeId
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}
//月请求数据
function Cmonth(obj){ 
    $('#date').val(obj) 
    console.info(obj)
    $.ajax({  
        type : "POST",  //提交方式
        url : business_statement_url,//路径  
        data : {  
            month:obj,
            store_id : storeId
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}
//年请求数据
function Cyear(obj){ 
    $('#date').val(obj) 
    console.info(obj)
    $.ajax({  
        type : "POST",  //提交方式
        url : business_statement_url,//路径  
        data : {  
            year:obj,
            store_id : storeId
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}

// ajax列表渲染
function renderData(obj){
    if(obj.errcode==1){
        $('#brankTit').text(obj.data.company_name)
        var total_price=parseFloat(obj.data.total_price)/10000
        var get_total_price=total_price.toFixed(2)
        $('#allPrice').text('￥'+get_total_price+'万')
        var package_list=obj.data.package_list;//获取到公司列表的数据
        var dataArr = [];
        // if(package_list.length==0){
        //     window.YDUI.dialog.toast('暂无数据', 'error', 1000,function(){
        //         dataArr.splice(0,dataArr.length);
        //     });
        // }
        $('.yd-package-list .yd-body').html('');
        for(var i=0;i<package_list.length;i++){
            var li='<div class="yd-tr">' +
                    '<div class="yd-l">' +
                        '<span class="yd-color" style="background:'+option_2.color[i]+'"></span>' +
                        '<span class="yd-title">'+package_list[i].name+'</span>' +
                    '</div>' +
                    '<div class="yd-c">'+package_list[i].count_package+'</div>' +
                    '<div class="yd-r">'+(package_list[i].count_package/obj.data.total_meal*100).toFixed(2)+'%</div>' +
                '</div>';
            $('.yd-package-list .yd-body').append(li);
            if(package_list[i].name.indexOf("(")>0){
                package_list[i].name =  insert_flg(package_list[i].name,'\n',package_list[i].name.indexOf("("));
             }
            if(package_list[i].name.indexOf("（")>0){
                package_list[i].name =  insert_flg(package_list[i].name,'\n',package_list[i].name.indexOf("（"));
             }
            dataArr[i]=new addObj(package_list[i].name,package_list[i].count_package);   
        }
        if(obj.data.total_meal==0){
            $('.yd-Echart-2').css('height','3rem');
            $('#my-Echart-2').hide();
            $('.yd-Echart-2 .tip').show();
            option_2.title.text='暂无签单'
        }else{
            $('.yd-Echart-2').css('height','6.18rem');
            $('#my-Echart-2').show();
            $('.yd-Echart-2 .tip').hide();
            option_2.title.text='共'+obj.data.total_meal+'单'
        }
        option_2.series[0].data=dataArr;//赋值到图表的数组
        // 使用刚指定的配置项和数据显示图表。
        myChart_2.setOption(option_2);
        // 设置柱状图
        var package_price=(parseFloat(obj.data.patterning.package_price)/10000).toFixed(2);
        var project_price=(parseFloat(obj.data.patterning.project_price)/10000).toFixed(2);
        var confinement_price=(parseFloat(obj.data.patterning.confinement_price)/10000).toFixed(2);
        var goods_price=(parseFloat(obj.data.patterning.goods_price)/10000).toFixed(2);
        var refund=(parseFloat(obj.data.patterning.refund)/10000).toFixed(2);
        option_1.series[0].data.splice(0,option_1.series[0].data.length);
        option_1.series[0].data.push(package_price)
        option_1.series[0].data.push(project_price)
        option_1.series[0].data.push(confinement_price)
        option_1.series[0].data.push(goods_price)
        option_1.series[0].data.push(refund)
        // 使用刚指定的配置项和数据显示图表。
        myChart_1.setOption(option_1);
        // 限制next按鈕不能超出當前時間，超出隱藏按鈕，否則顯示
        if($('#date').val()==currentDay){
            if($('.c_day ').hasClass('active')){
                $('.nextDay').hide()
                return
            }
        }
        if($('input[name=getCurrentWeek]').val()==getWeek){
            if($('.c_week ').hasClass('active')){
            $('.nextDay').hide()
            return
            }
        }
        if($('#date').val()==y+"-"+m){
            if($('.c_month ').hasClass('active')){
            $('.nextDay').hide()
            return
            }
        }
        if($('#date').val()==y){
            if($('.c_year').hasClass('active')){
            $('.nextDay').hide()
            return
            }
        }
        $('.nextDay').show()
    }else{
        window.YDUI.dialog.toast(obj.errinfo, 'error', 2000);
    }  
}
