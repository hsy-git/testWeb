// var myChart = echarts.init(document.getElementById('my-Echart'));// 饼图id
var worldMapContainer = document.getElementById('my-Echart');
//用于使chart自适应高度和宽度,通过窗体高宽计算容器高宽
var resizeWorldMapContainer = function () {
    worldMapContainer.style.width = window.innerWidth+'px';
    worldMapContainer.style.height = (window.innerHeight)/2+'px';
};
//设置容器高宽
resizeWorldMapContainer();
// 基于准备好的dom，初始化echarts实例
var myChart = echarts.init(worldMapContainer);
var getWeek=0;
var oneLoadNum = 0;

//图表
option = {
    grid: {
        left: '0',
        right: '0',
        bottom: '0',
        top:'15%',
        containLabel: true
    },
    legend: {
        height: worldMapContainer.style.height,
        width: worldMapContainer.style.width
    },
    series: [
        {
            name:'销售报表',
            type:'pie',
            radius: ['20%', '40%'],
            data:[],
            color: [
                      '#646bbc','#8489c9','#a3a6d9','#c1c4e3','#31bee9',
                       '#5bc6e8','#7fd9f4','#c4e8f3','#F3A43B','#60C0DD',
                       '#D7504B','#C6E579','#F4E001','#F0805A','#26C0C0'
                    ],
            itemStyle: {
                normal:{
                    label:{
                        show:true,
                        formatter: '{b}\n {c}\n{d}%'
                    },
                    labelLine:{
                        show:true
                    }
                }    
            },
            label: {
                normal: {
                    formatter: '{b}\n {c}万\n{d}%',
                    //color:'#444',
                    borderWidth: 1,
                    padding: [0, 2],
                    rich: {
                        b: {
                            fontSize: 10,
                            lineHeight: 30
                        },
                    }
                }
            }
        }
    ]
};
$(function(){
    //初始化获取当前日请求页面数据
    Cday(currentDay);
    var getaURL=GetQueryString('userId')
    $('.yd-tab-list li:nth-child(1) a').attr('href','brank.html?userId='+getaURL+'');
    //点击日，周，月，年
    $('.yd-select-date button').on('click',function(){
        oneLoadNum=1;
        ToggleTab($(this));
        var index=$(this).index();
        // 点击日按钮
        if(index==0){
        	$('.currentStatus').text('日');
        	Cday(currentDay);//请求日数据
        // 点击周按钮	
        }else if(index==1){
        	$('.currentStatus').text('周');
        	get_week()//请求周数据
        // 点击月按钮  
        }else if(index==2){
        	$('.currentStatus').text('月');
            Cmonth(y+"-"+m)//请求月数据
        // 点击年按钮
        }else if(index==3){
        	$('.currentStatus').text('年');
            Cyear(y)//请求年数据
        }
    })
    //切换排行榜
    $('.yd-tab-list li').on('click',function(){
        var index=$(this).index();
        if(index==1||index==2){
            window.YDUI.dialog.toast('正在开发中!','error', 1000);
            return
        }
    })
})

//----------------------------------------------------method--------------------------------------------------------------------------------------------

// prev
function prevDay(){
    //如果是日
    if($('.c_day').hasClass('active')){
        addDate($('#date').val(),-1);
        Cday($('#date').val());
        return
    }
    //如果是周
    if($('.c_week').hasClass('active')){
        var getWeek=parseInt($('input[name=getCurrentWeek]').val())-1;
        transformAjax(getWeek)
        if($('input[name=getCurrentWeek]').attr('value')==getWeek){
            $('.nextDay').hide() 
        }else{
            $('.nextDay').show()
        }
        return
    }
    //如果是月
    if($('.c_month').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
    	var str=dateToString(AddMonths(myDate,-1));
    	var getMonth=str.substr(0,7);
        
    	Cmonth(getMonth)
        return
    }
    //如果是年
    if($('.c_year').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
    	var str=dateToString(AddYear(myDate,-1));
    	var getYear=str.substr(0,4);
    	Cyear(getYear)
         if($('#date').val()==y){
           $('.nextDay').hide() 
       }else{
            $('.nextDay').show()
       }
        return
    }
}
//next
function nextDay(){
    //如果是日
    if($('.c_day').hasClass('active')){
        console.info()
        addDate($('#date').val(),1);
        Cday($('#date').val())
        return
    }
    //如果是周
    if($('.c_week').hasClass('active')){
        var getWeek=parseInt($('input[name=getCurrentWeek]').val())+1;
        transformAjax(getWeek)
        return
    }
    //如果是月
    if($('.c_month').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
    	var str=dateToString(AddMonths(myDate,1));
    	var getMonth=str.substr(0,7);
    	Cmonth(getMonth)
        return
    }
    //如果是年
    if($('.c_year').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
    	var str=dateToString(AddYear(myDate,1));
    	var getYear=str.substr(0,4);
    	Cyear(getYear)
        return
    }
}
 //月份增减
function AddMonths(date, value) {
    date.setMonth(date.getMonth() + value);
    return date
}
 //年份增减
function AddYear(date, value) {
    date.setFullYear(date.getFullYear() + value);
    return date
}
//时间格式转化字符串
function dateToString(now){  
    var year = now.getFullYear();  
    var month =(now.getMonth() + 1).toString();  
    var day = (now.getDate()).toString();  
    var hour = (now.getHours()).toString();  
    var minute = (now.getMinutes()).toString();  
    var second = (now.getSeconds()).toString();  
    if (month.length == 1) {  
        month = "0" + month;  
    }  
    if (day.length == 1) {  
        day = "0" + day;  
    }  
    if (hour.length == 1) {  
        hour = "0" + hour;  
    }  
    if (minute.length == 1) {  
        minute = "0" + minute;  
    }  
    if (second.length == 1) {  
        second = "0" + second;  
    }  
     var dateTime = year + "-" + month + "-" + day +" "+ hour +":"+minute+":"+second;  
     return dateTime;  
}

//切换周期
function transformAjax(Week){
    var 
        date=$('#date').val();
        Sweek=date.substr(0,10);
        Eweek=date.substr(11,10);
    $.ajax({  
        type : "POST",  //提交方式
        url : get_week_url,//路径  
        data : {
            week: Week,
            sweek:Sweek,
            eweek:Eweek

        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            if(result.errcode==1){
                var getWeek=result.data.week;
                $('input[name=getCurrentWeek]').val(getWeek)
            }
            var sweek=result.data.weeks[0];
            var eweek=result.data.weeks[1];
            $('#date').val(sweek+'至'+eweek);
            Cweek($('#date'))//周请求数据
        }  
    });
}

//日请求数据
function Cday(dayData){
    $('#date').val(dayData);
    $.ajax({  
        type : "POST",  //提交方式
        url : summary_sheet_url,//路径  
        data : {  
            userId:GetQueryString('userId'),
            day : dayData 
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}
//获取周
function get_week(){
	$.ajax({  
        type : "POST",  //提交方式
        url : get_week_url,//路径  
        data : { 
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            if(result.errcode==1){
                getWeek=result.data.week;
                $('input[name=getCurrentWeek]').val(getWeek);
            }
            var sweek_0=result.data.weeks[0];
            var eweek_1=result.data.weeks[1];
            $('#date').val(sweek_0+'至'+eweek_1);
            Cweek($('#date'))//周请求数据
        }  
    });
}
//周请求数据
function Cweek(obj){ 	
	console.info(obj.val())
    var 
    	globalWeek=obj.val();
        Sweek=globalWeek.substr(0,10);
        Eweek=globalWeek.substr(11,10);
    $.ajax({  
        type : "POST",  //提交方式
        url : summary_sheet_url,//路径  
        data : {  
            sweek:Sweek,
            eweek:Eweek,
            userId:GetQueryString('userId'),
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}
//月请求数据
function Cmonth(obj){ 
	$('#date').val(obj)	
    $.ajax({  
        type : "POST",  //提交方式
        url : summary_sheet_url,//路径  
        data : {  
            month:obj,
            userId:GetQueryString('userId'),
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}
//年请求数据
function Cyear(obj){ 
	$('#date').val(obj)	
    $.ajax({  
        type : "POST",  //提交方式
        url : summary_sheet_url,//路径  
        data : {  
            year:obj,
            userId:GetQueryString('userId'),
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}

// ajax列表渲染
function renderData(obj){
	if(obj.errcode==1){
        var getPrice=parseFloat(obj.data.order_total)/10000;//获取当日营业额
        var getOrder=obj.data.sign_num;//获取签单人数
        var getTime=h+":"+t;//获取当前时间
        $('#price').text('￥'+getPrice.toFixed(2));
        $('#order').text(getOrder);
        $('#time').text(getTime);
        var companyList=obj.data.company;//获取到公司列表的数据
        var dataArr = [];
        $('.yd-brank-list').html('');
        var total_price_arr=[];
        // if(companyList.length==0){
        //     $('#my-Echart').find('div').text('暂无数据');
        //     return
        // }
        // $('#my-Echart').find('div').text('');
        for(var i=0;i<companyList.length;i++){
            var li='<li><a href="brankFrom.html?store_id='+companyList[i].store_id+'&userId='+GetQueryString('userId')+'"><div><img src="img/form_ic_pin.png" alt=""></div><span>'+companyList[i].company_name+'</span></a></li>';
            $('.yd-brank-list').append(li);
            if(companyList[i].price!=''){
                dataArr[i]=new addObj(companyList[i].company_name,(parseFloat(companyList[i].price)/10000).toFixed(2))
            }
            total_price_arr.push(parseFloat(companyList[i].price/10000).toFixed(2));
        }
        if(parseFloat(eval(total_price_arr.join("+"))).toFixed(2)==0.00){
            $('#my-Echart').css('height','3rem');
            $('#my-Echart').find('div').hide();
            $('.yd-Echart').find('b').show();
            $('.yd-Echart').find('b').text('暂无数据');
            confineDate()
            return
        }
        $('#my-Echart').css('height','6.8rem');
        $('#my-Echart').find('div').show();
        $('.yd-Echart').find('b').hide();
        option.series[0].data=dataArr;//赋值到图表的数组
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        //用于使chart自适应高度和宽度
        window.onresize = function () {
            //重置容器高宽
            resizeWorldMapContainer();
            myChart.resize();
        };

        confineDate()
    } else{
        window.YDUI.dialog.toast(obj.errinfo, 'error', 2000);
    } 
}
