//brank.js
var getWeek=0;
var oneLoadNum = 0;
$(function(){
    //初始化获取当前日请求页面数据
    Cday(currentDay);
    //点击日，周，月，年
    $('.yd-select-date button').on('click',function(){
        oneLoadNum=1;
        ToggleTab($(this));
        var index=$(this).index();
        // 点击日按钮
        if(index==0){
        	$('.currentStatus').text('日');
        	Cday(currentDay);//请求日数据
        // 点击周按钮	
        }else if(index==1){
        	$('.currentStatus').text('周');
        	get_week()//请求周数据
        // 点击月按钮  
        }else if(index==2){
        	$('.currentStatus').text('月');
            Cmonth(y+"-"+m)//请求月数据
        // 点击年按钮
        }else if(index==3){
        	$('.currentStatus').text('年');
            Cyear(y)//请求年数据
        }
    })
})

//----------------------------------------------------method--------------------------------------------------------------------------------------------
// prev
function prevDay(){
    //如果是日
    if($('.c_day').hasClass('active')){
        addDate($('#date').val(),-1);
        Cday($('#date').val())
        return
    }
    //如果是周
    if($('.c_week').hasClass('active')){
        var getWeek=parseInt($('input[name=getCurrentWeek]').val())-1;
        transformAjax(getWeek)
        return
    }
    //如果是月
    if($('.c_month').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
    	var str=dateToString(AddMonths(myDate,-1));
    	var getMonth=str.substr(0,7);
    	Cmonth(getMonth)
        return
    }
    //如果是年........
    if($('.c_year').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
    	var str=dateToString(AddYear(myDate,-1));
    	var getYear=str.substr(0,4);
    	Cyear(getYear)
        return
    }

}
//next
function nextDay(){
    //如果是日
    if($('.c_day').hasClass('active')){
        addDate($('#date').val(),1);
        Cday($('#date').val())
        return
    }
    //如果是周
    if($('.c_week').hasClass('active')){
        var getWeek=parseInt($('input[name=getCurrentWeek]').val())+1;
        transformAjax(getWeek)
        return
    }
    //如果是月
    if($('.c_month').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
    	var str=dateToString(AddMonths(myDate,1));
    	var getMonth=str.substr(0,7);
    	Cmonth(getMonth)
        return
    }
    //如果是年
    if($('.c_year').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
    	var str=dateToString(AddYear(myDate,1));
    	var getYear=str.substr(0,4);
    	Cyear(getYear)
        return
    }
}
 //月份增减
function AddMonths(date, value) {
    date.setMonth(date.getMonth() + value);
    return date;
}
 //年份增减
function AddYear(date, value) {
    date.setFullYear(date.getFullYear() + value);
    return date;
}
//时间格式转化字符串
function dateToString(now){  
    var year = now.getFullYear();  
    var month =(now.getMonth() + 1).toString();  
    var day = (now.getDate()).toString();  
    var hour = (now.getHours()).toString();  
    var minute = (now.getMinutes()).toString();  
    var second = (now.getSeconds()).toString();  
    if (month.length == 1) {  
        month = "0" + month;  
    }  
    if (day.length == 1) {  
        day = "0" + day;  
    }  
    if (hour.length == 1) {  
        hour = "0" + hour;  
    }  
    if (minute.length == 1) {  
        minute = "0" + minute;  
    }  
    if (second.length == 1) {  
        second = "0" + second;  
    }  
     var dateTime = year + "-" + month + "-" + day +" "+ hour +":"+minute+":"+second;  
     return dateTime;  
}
//切换周期
function transformAjax(Week){
    var 
        date=$('#date').val();
        Sweek=date.substr(0,10);
        Eweek=date.substr(11,10);
    $.ajax({  
        type : "POST",  //提交方式
        url : get_week_url,//路径  
        data : {
            week: Week,
            sweek:Sweek,
            eweek:Eweek

        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            if(result.errcode==1){
                var getWeek=result.data.week;
                $('input[name=getCurrentWeek]').val(getWeek)
            }
            var sweek=result.data.weeks[0];
            var eweek=result.data.weeks[1];
            $('#date').val(sweek+'至'+eweek);
            Cweek($('#date'))//周请求数据
        }  
    });
}
//日请求数据
function Cday(dayData){
    $('#date').val(dayData);
    $.ajax({  
        type : "POST",  //提交方式
        url : summary_sheet_url,//路径  
        data : {  
            userId:GetQueryString('userId'),
            day : dayData 
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}
//获取周
function get_week(){
	$.ajax({  
        type : "POST",  //提交方式
        url : get_week_url,//路径  
        data : { 
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            if(result.errcode==1){
                getWeek=result.data.week;
                $('input[name=getCurrentWeek]').val(getWeek)
            }
            var sweek_0=result.data.weeks[0];
            var eweek_1=result.data.weeks[1];
            $('#date').val(sweek_0+'至'+eweek_1);
            Cweek($('#date'))//周请求数据
        }  
    });
}
//周请求数据
function Cweek(obj){ 	
	console.info(obj.val())
    var 
    	globalWeek=obj.val();
        Sweek=globalWeek.substr(0,10);
        Eweek=globalWeek.substr(11,10);
    $.ajax({  
        type : "POST",  //提交方式
        url : summary_sheet_url,//路径  
        data : {  
            sweek:Sweek,
            eweek:Eweek,
            userId:GetQueryString('userId'),
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}
//月请求数据
function Cmonth(obj){ 
	$('#date').val(obj)	
	console.info(obj)
    $.ajax({  
        type : "POST",  //提交方式
        url : summary_sheet_url,//路径  
        data : {  
            month:obj,
            userId:GetQueryString('userId'),
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}
//年请求数据
function Cyear(obj){ 
	$('#date').val(obj)	
	console.info(obj)
    $.ajax({  
        type : "POST",  //提交方式
        url : summary_sheet_url,//路径  
        data : {  
            year:obj,
            userId:GetQueryString('userId'),
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}

// ajax列表渲染
function renderData(obj){
	if(obj.errcode==1){
        var total_price_arr=[];
        var companyList=obj.data.company;//获取到公司列表的数据
        $('.yd-turnover-list').html('');
        // if(companyList.length==0){
        //     window.YDUI.dialog.toast('暂无数据', 'error', 1000,function(){
        //         dataArr.splice(0,dataArr.length);
        //     });
        // }
        for(var i=0;i<companyList.length;i++){
            var $html='<li>' +
		                '<div class="yd-l">'+(i+1)+'. '+companyList[i].company_name+'</div>' +
		                '<div class="yd-r">'+(companyList[i].price/10000).toFixed(2)+'万</div>' +
		            '</li>';
            total_price_arr.push(parseFloat(companyList[i].price/10000).toFixed(2));
            $('.yd-turnover-list').append($html)            
        }
        $('.yd-all-price').text(parseFloat(eval(total_price_arr.join("+"))).toFixed(2))
        // 限制next按鈕不能超出當前時間，超出隱藏按鈕，否則顯示
        if($('#date').val()==currentDay){
            if($('.c_day ').hasClass('active')){
                $('.nextDay').hide()
                return
            }
        }
        if($('input[name=getCurrentWeek]').val()==getWeek){
            if($('.c_week ').hasClass('active')){
            $('.nextDay').hide()
            return
            }
        }
        if($('#date').val()==y+"-"+m){
            if($('.c_month ').hasClass('active')){
            $('.nextDay').hide()
            return
            }
        }
        if($('#date').val()==y){
            if($('.c_year').hasClass('active')){
            $('.nextDay').hide()
            return
            }
        }
        $('.nextDay').show()
    }else{
        window.YDUI.dialog.toast(obj.errinfo, 'error', 2000);
    } 
}