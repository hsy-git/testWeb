// 通用.js	
var url = 'http://erp.shixinyuezi.net/';
var summary_sheet_url= url+'index.php/ShixinApi/OnlineMarketing/summary_sheet';//获取汇总表数据接口
var shop_statement_url= url+'index.php/ShixinApi/OnlineMarketing/shop_statement';//获取门店报表接口
var night_paper_url= url+'index.php/ShixinApi/OnlineMarketing/night_paper';//获取晚报接口
var market_statement_url= url+'index.php/ShixinApi/OnlineMarketing/market_statement';//获取营销报表接口
var business_statement_url= url+'index.php/ShixinApi/OnlineMarketing/business_statement';//获取营业报表接口
var get_week_url= url+'index.php/ShixinApi/OnlineMarketing/get_week';//获取周接口
var get_company_name= url+'index.php/ShixinApi/OnlineMarketing/get_company_name';//获取公司接口

var myDate = new Date();//获取系统当前时间
var y=myDate.getFullYear(); //获取完整的年份(4位,1970-????)
var m=myDate.getMonth()+1; //获取当前月份(0-11,0代表1月)
var d=myDate.getDate(); //获取当前日(1-31)
var h=myDate.getHours();//获取小时
var t=myDate.getMinutes()//获取分钟
if(t<10) t='0'+t;//判断分钟小于10的时候前面补个0
if(h<10) h='0'+h;//判断小时小于10的时候前面补个0
if(m<10) m='0'+m;//判断月小于10的时候前面补个0
if(d<10)d='0'+d;//判断日小于10的时候前面补个0
var currentDay=y+'-'+m+'-'+d//当前日期    
var t,n,count = 0;  
//天数增减
function addDate(date,days){ 
   var d=new Date(date); 
   d.setDate(d.getDate()+days); 
   var m=d.getMonth()+1; 
   var day=d.getDate();
   if(m<10){
    m='0'+m;
   }
   if(day<10){
    day='0'+day
   }
   var newDate=d.getFullYear()+'-'+m+'-'+day; 
   $('#date').val(newDate)
}
//切换类方法
function ToggleTab(obj){
    obj.addClass('active').siblings().removeClass('active')
}

//获取URL
function getarg(url){
    arg=url.split("=");
    return arg[1];
} 
//添加对象
function addObj(name,value){
    this.name=name;
    this.value=value;
}
//str表示原字符串变量，flg表示要插入的字符串，sn表示要插入的位置
function insert_flg(str,flg,sn){
    var newstr="";
    for(var i=0;i<str.length;i+=sn){
        var tmp=str.substring(i, i+sn);
        newstr+=tmp+flg;
    }
    return newstr;
}
function getDay() {
     var value = $("#date").val().trim();
     if (value == "") {
         return;
     } else {
         var day = new Date(value).getDay(),
             text = "";
         switch (day) {
             case 0:
                 text = "日";
                 break;
             case 1:
                 text = "一";
                 break;
             case 2:
                 text = "二";
                 break;
             case 3:
                 text = "三";
                 break;
             case 4:
                 text = "四";
                 break;
             case 5:
                 text = "五";
                 break;
             case 6:
                 text = "六";
                 break;
         }
         $('#week').text(text)
     }
 }

//获取URL中的参数
function GetQueryString(name) {  
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");  
    var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
    var context = "";  
    if (r != null)  
         context = r[2];  
    reg = null;  
    r = null;  
    return context == null || context == "" || context == "undefined" ? "" : context;  
}


function showAuto(){
    var leftNum = $('#my-Echart div:nth-child(2)').css('left');
    var leftNum = parseInt(leftNum);
    if(leftNum<0){
        $('#my-Echart div:nth-child(2)').css('left','10px');
    }
}

//str表示原字符串变量，flg表示要插入的字符串，sn表示要插入的位置
function insert_flg(str,flg,sn){
    var newstr="";
    for(var i=0;i<str.length;i+=sn){
        var tmp=str.substring(i, i+sn);
        newstr+=tmp+flg;
    }
    return newstr;
}


function confineDate(){
    // 限制next按鈕不能超出當前時間，超出隱藏按鈕，否則顯示
    if($('#date').val()==currentDay){
        if($('.c_day ').hasClass('active')){
            $('.nextDay').hide()
            return
        }
    }
    if($('input[name=getCurrentWeek]').val()==getWeek){
        if($('.c_week ').hasClass('active')){
        $('.nextDay').hide()
        return
        }
    }
    if($('#date').val()==y+"-"+m){
        if($('.c_month ').hasClass('active')){
        $('.nextDay').hide()
        return
        }
    }
    if($('#date').val()==y){
        if($('.c_year').hasClass('active')){
        $('.nextDay').hide()
        return
        }
    }
    $('.nextDay').show()
}
