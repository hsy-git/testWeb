var myChart = echarts.init(document.getElementById('my-Echart'));// 柱状图
var thisURL = document.URL;
var storeId=getarg(thisURL);//获取公司ID
// 图表
option = {
    tooltip : {
        trigger: 'item',
        formatter: "{c}"
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '10%',
        top:'10%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : [],
            // 控制网格线是否显示
            splitLine:{
                show: true, 
                //  改变轴线颜色
                lineStyle: {
                    // 使用深浅的间隔色
                    color: ['#e3e3e3']
                }                            
            },
            //  改变x轴颜色
            axisLine:{
                lineStyle:{
                    color:'#ffffff',
                }
            },                         
            //  改变x轴字体颜色和大小
            axisLabel: {
                textStyle: {
                    color: '#aeb6bb',
                    fontSize:'10'
                },
            }    
        }
    ],  
    yAxis : [
        {
            type : 'value',
            //  改变x轴颜色
            axisLine:{
                show: true,
                lineStyle:{
                    color:'#e3e3e3',
                }
            },                         
            //  改变y轴字体颜色和大小
            axisLabel: {
                textStyle: {
                    color: '#aeb6bb',
                    fontSize:'10'
                },
                formatter: '{value}万'
            }   
        }
    ],
    series : [
        {
            name:'月子套餐实收金额',
            type:'line',
            stack: '金额',
            areaStyle: {normal: {
                color:'#fdf4f3'
            }},
            data:[],
            itemStyle:{ 
                normal:{ 
                    label:{ 
                       show: true, 
                       formatter: '{c}万' 
                    }, 
                    labelLine :{show:true}
                } 
            } 
        }
    ]
};

$(function(){
    $('#date').val(currentDay)//初始化获取当天日期
    Cday(currentDay)//初始化请求当天数据
})

//-----------------------------------method--------------------------------------------------------------
// prev
function prevDay(){
    addDate($('#date').val(),-1);
    Cday($('#date').val())
}
//next
function nextDay(){
    addDate($('#date').val(),1);
    Cday($('#date').val())
}

//初始化请求数据
function Cday(dayData){
    console.info(dayData)
    $('#date').val(dayData);
    $.ajax({  
        type : "POST",  //提交方式
        url : night_paper_url,//路径  
        data : {  
            day : dayData,
            store_id : storeId
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result) 
            if(result.errcode==1){ 
                getDay()

                $('#brankTit').text(result.data.company_name);
                $('#data_0').text(result.data.company_name)
                $('#data_1').text(result.data.day_sign);
                $('#data_2').text(result.data.last_sign);
                $('#data_3').text(result.data.yesterday_sign);
                $('#data_4').html(result.data.consult_people+'<span style="padding-left:0.1rem">(有效率：'+result.data.effective_rate+'%)</span>');
                $('#data_5').text(result.data.advanced_order);
                $('#data_6').text(result.data.live_sure);
                $('#data_7').text(result.data.advanced_price); 
                var actual_priceList= result.data.actual_price;
                var moneyArr = new Array;
                var payTimeArr= new Array;
                // if(actual_priceList.length==0){
                //     window.YDUI.dialog.toast('暂无数据', 'error', 1000,function(){
                //         moneyArr.splice(0,moneyArr.length);//清空数组
                //         payTimeArr.splice(0,payTimeArr.length);
                //     });
                // }
                for(var i=0;i<actual_priceList.length;i++){
                    moneyArr.push((parseFloat(actual_priceList[i].money)/10000).toFixed(2));  
                    payTimeArr.push(actual_priceList[i].pay_time);  
                }
                option.xAxis[0].data=payTimeArr;//赋值到图表的数组
                option.series[0].data=moneyArr;//赋值到图表的数组
                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                // 限制next按鈕不能超出當前時間，超出隱藏按鈕，否則顯示
                if($('#date').val()==currentDay){
                    $('.nextDay').hide()
                }else{
                    $('.nextDay').show()
                }
            }else{
                window.YDUI.dialog.toast(result.errinfo, 'error', 2000);
            } 
        }
    });
}

