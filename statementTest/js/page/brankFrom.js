var worldMapContainer = document.getElementById('my-Echart');
//用于使chart自适应高度和宽度,通过窗体高宽计算容器高宽
var resizeWorldMapContainer = function () {
    worldMapContainer.style.width = window.innerWidth+'px';
    worldMapContainer.style.height = (window.innerHeight)/2+'px';
};
//设置容器高宽
resizeWorldMapContainer();
// 基于准备好的dom，初始化echarts实例
var myChart = echarts.init(worldMapContainer);
var thisURL = document.URL;
var storeId=GetQueryString('store_id');//获取公司ID
var getWeek=0;
var oneLoadNum = 0;
var currentWeek=0
//图表
option = {
    grid: {
        left: '0',
        right: '0',
        bottom: '0',
        top:'15%',
        containLabel: true
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c}单"
    },
    legend: {
        height: worldMapContainer.style.height/2,
        width: worldMapContainer.style.width
    },
    series: [
        {
            name:'套餐分布',
            type:'pie',
            radius: ['20%', '40%'],
            data:[],
            color: [
                      '#646bbc','#8489c9','#a3a6d9','#c1c4e3','#31bee9',
                       '#5bc6e8','#7fd9f4','#c4e8f3','#F3A43B','#60C0DD',
                       '#D7504B','#C6E579','#F4E001','#F0805A','#26C0C0'
                    ],
            formatter: function(b){
                if(b.data.name.indexOf(")")>0){
                    return insert_flg(b.data.name,'\n',b.data.name.indexOf(")"));
                }
                return b.data.name;
            },
            label: {
                normal: {
                    // formatter:"{b}"
                   
                    //color:'#444',
                    borderWidth: 1,
                    // padding: [5, 5],
                    rich: {
                        b: {
                            fontSize: 10,
                            lineHeight: 20
                        },
                    }
                }
            }
        }
    ]
}
$(function(){
    t = setInterval(showAuto, 1000); 
    initCompanyList()
    //初始化获取当前日请求页面数据
    Cday(currentDay);
    //点击日，周，月，年
    $('.yd-select-date button').on('click',function(){
        oneLoadNum=1;
        ToggleTab($(this));
        var index=$(this).index();
        // 点击日按钮
        if(index==0){
            $('.currentStatus').text('日');
            Cday(currentDay);//请求日数据
        // 点击周按钮    
        }else if(index==1){
            $('.currentStatus').text('周');
            get_week()//请求周数据
        // 点击月按钮  
        }else if(index==2){
            $('.currentStatus').text('月');
            Cmonth(y+"-"+m)//请求月数据
        // 点击年按钮
        }else if(index==3){
            $('.currentStatus').text('年');
            Cyear(y)//请求年数据
        }
    })
    //打开分店上拉菜单
    $('#J_ShowActionSheet').on('click', function () {
        $('#J_ActionSheet').actionSheet('open');
    });
    //关闭分店上拉菜单
    $('#J_Cancel').on('click', function () {
        $('#J_ActionSheet').actionSheet('close');
    });
    //点击分店
    $('body').on('click','#J_ActionSheet .actionsheet-item',function(){
        $(this).addClass('active').siblings('.actionsheet-item').removeClass('active');//切换类
        $('#brankTit').text($(this).text());//赋值到分店标题
        storeId=$(this).attr('id');//获取公司id
        console.info(storeId)
        $('.yd-brank-menu li').each(function(){
            var strUrl=$(this).find('a').attr('href');
            var curUrl=strUrl.substr(0,13);
            $(this).find('a').attr('href',curUrl+'?store_id='+storeId+'');
        })

        $('#J_ActionSheet').actionSheet('close');//关闭弹出窗
        if($('.c_day ').hasClass('active')){
            Cday(currentDay);//请求日数据
            return
        }
        if($('.c_week ').hasClass('active')){
            get_week()//请求周数据
            return
        }
        if($('.c_month ').hasClass('active')){
            Cmonth(y+"-"+m)//请求月数据
            return
        }
        if($('.c_year ').hasClass('active')){
            Cyear(y)//请求年数据 
            return
        }
    })
})

//-----------------------------------method--------------------------------------------------------------
// prev
function prevDay(){
    //如果是日
    if($('.c_day').hasClass('active')){
        addDate($('#date').val(),-1);
        Cday($('#date').val())
        return
    }
    //如果是周
    if($('.c_week').hasClass('active')){
        var getWeek=parseInt($('input[name=getCurrentWeek]').val())-1;
        transformAjax(getWeek)
        if($('input[name=getCurrentWeek]').attr('value')==getWeek){
            $('.nextDay').hide() 
        }else{
            $('.nextDay').show()
        }
        return
    }
    //如果是月
    if($('.c_month').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
        var str=dateToString(AddMonths(myDate,-1));
        var getMonth=str.substr(0,7);
        Cmonth(getMonth)
        return
    }
    //如果是年
    if($('.c_year').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
        var str=dateToString(AddYear(myDate,-1));
        var getYear=str.substr(0,4);
        Cyear(getYear)
        return
    }
}
//next
function nextDay(){
    //如果是日
    if($('.c_day').hasClass('active')){
        addDate($('#date').val(),1);
        Cday($('#date').val())
        return
    }
    //如果是周
    if($('.c_week').hasClass('active')){
        var getWeek=parseInt($('input[name=getCurrentWeek]').val())+1;
        transformAjax(getWeek)
        return
    }
    //如果是月
    if($('.c_month').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
        var str=dateToString(AddMonths(myDate,1));
        var getMonth=str.substr(0,7);
        Cmonth(getMonth)
        return
    }
    //如果是年
    if($('.c_year').hasClass('active')){
        if(oneLoadNum==1){
            myDate = new Date();
            oneLoadNum = 0;
        }
        var str=dateToString(AddYear(myDate,1));
        var getYear=str.substr(0,4);
        Cyear(getYear)
        return
    }
}
 //月份增减
function AddMonths(date, value) {
    date.setMonth(date.getMonth() + value);
    return date;
}
 //年份增减
function AddYear(date, value) {
    date.setFullYear(date.getFullYear() + value);
    return date;
}
//时间格式转化字符串
function dateToString(now){  
    var year = now.getFullYear();  
    var month =(now.getMonth() + 1).toString();  
    var day = (now.getDate()).toString();  
    var hour = (now.getHours()).toString();  
    var minute = (now.getMinutes()).toString();  
    var second = (now.getSeconds()).toString();  
    if (month.length == 1) {  
        month = "0" + month;  
    }  
    if (day.length == 1) {  
        day = "0" + day;  
    }  
    if (hour.length == 1) {  
        hour = "0" + hour;  
    }  
    if (minute.length == 1) {  
        minute = "0" + minute;  
    }  
    if (second.length == 1) {  
        second = "0" + second;  
    }  
     var dateTime = year + "-" + month + "-" + day +" "+ hour +":"+minute+":"+second;  
     return dateTime;  
}
//切换周期
function transformAjax(Week){
    var 
        date=$('#date').val();
        Sweek=date.substr(0,10);
        Eweek=date.substr(11,10);
    $.ajax({  
        type : "POST",  //提交方式
        url : get_week_url,//路径  
        data : {
            week: Week,
            sweek:Sweek,
            eweek:Eweek

        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            if(result.errcode==1){
                var getWeek=result.data.week;
                $('input[name=getCurrentWeek]').val(getWeek)
            }
            var sweek=result.data.weeks[0];
            var eweek=result.data.weeks[1];
            $('#date').val(sweek+'至'+eweek);
            Cweek($('#date'))//周请求数据
        }  
    });
}

//日请求数据
function Cday(dayData){
    console.info(dayData)
    $('#date').val(dayData);
    $.ajax({  
        type : "POST",  //提交方式
        url : shop_statement_url,//路径  
        data : {  
            day : dayData,
            store_id : storeId
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}
//获取周
function get_week(){
    $.ajax({  
        type : "POST",  //提交方式
        url : get_week_url,//路径  
        data : { 
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            if(result.errcode==1){
                getWeek=result.data.week;
                currentWeek=result.data.week;
                $('input[name=getCurrentWeek]').val(getWeek)
            }
            var sweek_0=result.data.weeks[0];
            var eweek_1=result.data.weeks[1];
            $('#date').val(sweek_0+'至'+eweek_1);
            Cweek($('#date'))//周请求数据
        }  
    });
}


//周请求数据
function Cweek(obj){    
    console.info(obj.val())
    var 
        globalWeek=obj.val();
        Sweek=globalWeek.substr(0,10);
        Eweek=globalWeek.substr(11,10);
    $.ajax({  
        type : "POST",  //提交方式
        url : shop_statement_url,//路径  
        data : {  
            sweek:Sweek,
            eweek:Eweek,
            store_id : storeId
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}
//月请求数据
function Cmonth(obj){ 
    $('#date').val(obj) 
    console.info(obj)
    $.ajax({  
        type : "POST",  //提交方式
        url : shop_statement_url,//路径  
        data : {  
            month:obj,
            store_id : storeId,

        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}
//年请求数据
function Cyear(obj){ 
    $('#date').val(obj) 
    console.info(obj)
    $.ajax({  
        type : "POST",  //提交方式
        url : shop_statement_url,//路径  
        data : {  
            year:obj,
            store_id : storeId
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            renderData(result)
        }  
    });
}

// ajax列表渲染
function renderData(obj){
    if(obj.errcode==1){
        var getZx=obj.data.consult_man;//获取咨询数
        var getRzl=obj.data.live;//获取入住率
        var getQd=obj.data.sign_man;//获取今日签单
        var getMeal=obj.data.total_meal;//获取总套餐数
        var getPrice=obj.data.total_price;//获取营业额
        var twenty_eight=obj.data.twenty_eight;//28天套餐
        var forty_two=obj.data.forty_two;//42天套餐
        if(isNaN(getRzl)){
            getRzl='0.00%'
        }
        if(isNaN(twenty_eight)){
            twenty_eight='0'
        }
        if(isNaN(forty_two)){
            forty_two='0'
        }
        $('#zx').text(getZx);
        $('#zzl').text(getRzl+"%");
        $('#qd').text(getQd);
        $('#salePrice').text('￥'+(parseFloat(getPrice)/10000).toFixed(2)+'万');
        $('#package28').text((parseFloat(twenty_eight)/10000).toFixed(2)+'万')
        $('#package42').text((parseFloat(forty_two)/10000).toFixed(2)+'万')
        var package_list=obj.data.package_list;//获取到公司列表的数据
        var dataArr = [];
        if(package_list.length==0){
            $('#my-Echart').find('div').hide();
            $('.yd-Echart').css('height','3.7rem');
            $('#my-Echart').css('height','2.6rem');
            $('.yd-Echart').find('b').show();
            $('.yd-Echart').find('b').text('暂无数据');
            confineDate()
            return
        }
        $('.yd-Echart').css('height','6.9rem');
        $('#my-Echart').css('height','5.8rem');
        $('#my-Echart').find('div').show();
        $('.yd-Echart').find('b').hide();
        for(var i=0;i<package_list.length;i++){
            if(package_list[i].name.indexOf("(")>0){
                package_list[i].name =  insert_flg(package_list[i].name,'\n',package_list[i].name.indexOf("("));
             }
            if(package_list[i].name.indexOf("（")>0){
                package_list[i].name =  insert_flg(package_list[i].name,'\n',package_list[i].name.indexOf("（"));
             }
            dataArr[i]=new addObj(package_list[i].name,package_list[i].count_package);   
        }
        option.series[0].data=dataArr;//赋值到图表的数组
        
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);

        //用于使chart自适应高度和宽度
        window.onresize = function () {
            //重置容器高宽
            resizeWorldMapContainer();
            myChart.resize();
        };
        confineDate()
    } else{
        window.YDUI.dialog.toast(obj.errinfo, 'error', 2000);
    } 
}

//初始化加载公司名称列表
function initCompanyList(){
    $.ajax({  
        type : "POST",  //提交方式
        url : get_company_name,//路径  
        data : { 
            userId:GetQueryString('userId')
        },//数据，这里使用的是Json格式进行传输  
        success : function(result) {//返回数据根据结果进行相应的处理  
            console.info(result)
            if(result.errcode==1){
                var companyList=result.data;//获取到公司列表的数据
                $('#J_ActionSheet .actionsheet-item').remove();
                for(var i=0;i<companyList.length;i++){
                    var $a='<a href="javascript:;" id="'+companyList[i].id+'" class="actionsheet-item">'+companyList[i].company_name+'</a>';
                    $('#J_ActionSheet #J_Cancel').before($a); 
                    if(companyList[i].id==storeId){
                        $('#J_ActionSheet #'+storeId+'').addClass('active');
                        $('#brankTit').text($('#J_ActionSheet #'+storeId+'').text())
                    } 
                }
                //循环赋值公司ID
                $('.yd-brank-menu li').each(function(){
                    var href=$(this).find('a').attr('href');
                    $(this).find('a').attr('href',href+'?store_id='+storeId+'');
                })
            }else{
                window.YDUI.dialog.toast(result.errinfo, 'error', 2000);
            } 
        }  
    });
}



